import 'package:flutter/material.dart';
import 'package:shopping_mall_test/function/token_lib.dart';
import 'package:shopping_mall_test/pages/page_index.dart';
import 'package:shopping_mall_test/pages/page_login.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    int? memberId = await TokenLib.getMemberId();

    if (memberId == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    }
  }
}
