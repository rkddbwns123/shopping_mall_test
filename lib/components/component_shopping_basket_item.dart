import 'package:flutter/material.dart';
import 'package:shopping_mall_test/model/shopping_basket_item.dart';

class ComponentShoppingBasketItem extends StatelessWidget {
  const ComponentShoppingBasketItem({super.key, required this.shoppingBasketItem});

  final ShoppingBasketItem shoppingBasketItem;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 150,
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.grey,
            width: 2,
          )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset(shoppingBasketItem.imageAddress,width: 100, height: 100, fit: BoxFit.fill,),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text(shoppingBasketItem.productName, style: TextStyle(fontWeight: FontWeight.bold),),
              Text('원래 가격 : ${shoppingBasketItem.originPrice}￦', style: TextStyle(fontWeight: FontWeight.bold),),
              Text('할인 가격 : ${shoppingBasketItem.disCountPrice}￦', style: TextStyle(fontWeight: FontWeight.bold),),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                onPressed: () {},
                child: Icon(
                  Icons.delete_forever,
                ),
              ),
              Text('장바구니에서 삭제', style: TextStyle(fontWeight: FontWeight.bold),)
            ],
          )
        ],
      ),
    );
  }
}
