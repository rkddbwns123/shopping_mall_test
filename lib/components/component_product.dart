import 'package:flutter/material.dart';

class ComponentProduct extends StatelessWidget {
  const ComponentProduct({super.key, required this.imageName, required this.textName, required this.productPrice});

  final String imageName;
  final String textName;
  final String productPrice;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          Image.asset(imageName, width: 100, height: 100,fit: BoxFit.fill),
          Text(textName),
          Text(productPrice)
        ],
      ),
    );
  }
}
