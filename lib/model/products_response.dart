import 'package:shopping_mall_test/model/products_detail_item.dart';

class ProductsResponse {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<ProductsDetailItem> list;

  ProductsResponse(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory ProductsResponse.fromJson(Map<String, dynamic> json) {
    return ProductsResponse(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'] ,
        json['list'] == null ? [] : (json['list'] as List).map((e) => ProductsDetailItem.fromJson(e)).toList()
    );
  }
}