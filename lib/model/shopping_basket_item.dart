class ShoppingBasketItem {
  String categoryName;
  int disCountPrice;
  int id;
  String imageAddress;
  int originPrice;
  String productName;

  ShoppingBasketItem(this.categoryName, this.disCountPrice, this.id, this.imageAddress, this.originPrice, this.productName);

  factory ShoppingBasketItem.fromJson(Map<String, dynamic> json) {
    return ShoppingBasketItem(
      json['categoryName'],
      json['disCountPrice'],
      json['id'],
      json['imageAddress'],
      json['originPrice'],
      json['productName']
    );
  }
}