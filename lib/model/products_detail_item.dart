class ProductsDetailItem {
  int id;
  String productName;
  String imageAddress;
  String categoryName;
  String originPrice;
  String disCountPrice;

  ProductsDetailItem(this.id, this.productName, this.imageAddress, this.categoryName, this.originPrice, this.disCountPrice);

  factory ProductsDetailItem.fromJson(Map<String, dynamic> json) {
    return ProductsDetailItem(
        json['id'],
        json['productName'],
        json['imageAddress'],
        json['categoryName'],
        json['originPrice'],
        json['disCountPrice']);
  }
}