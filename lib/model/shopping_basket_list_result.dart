import 'package:shopping_mall_test/model/shopping_basket_item.dart';

class ShoppingBasketListResult {
  int code;
  int currentPage;
  bool isSuccess;
  String msg;
  int totalItemCount;
  int totalPage;
  List<ShoppingBasketItem> list;

  ShoppingBasketListResult(this.code, this.currentPage, this.isSuccess, this.msg, this.totalItemCount, this.totalPage, this.list);

  factory ShoppingBasketListResult.fromJson(Map<String, dynamic> json) {
    return ShoppingBasketListResult(
      json['code'],
      json['currentPage'],
      json['isSuccess'] as bool,
      json['msg'],
      json['totalItemCount'],
      json['totalPage'],
      json['list'] == null ? [] : (json['list'] as List).map((e) => ShoppingBasketItem.fromJson(e)).toList()
    );
  }
}