import 'package:bot_toast/bot_toast.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:shopping_mall_test/components/common/component_custom_loading.dart';
import 'package:shopping_mall_test/components/common/component_notification.dart';
import 'package:shopping_mall_test/components/component_banner_item.dart';
import 'package:shopping_mall_test/components/component_product.dart';
import 'package:shopping_mall_test/model/products_detail_item.dart';
import 'package:shopping_mall_test/repository/repo_banners.dart';
import 'package:shopping_mall_test/repository/repo_products.dart';

class PageShoppingProduct extends StatefulWidget {
  const PageShoppingProduct({Key? key}) : super(key: key);

  @override
  State<PageShoppingProduct> createState() => _PageShoppingProductState();
}

class _PageShoppingProductState extends State<PageShoppingProduct> {
  List<String> imgList = [];

  Future<void> _getBannerList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBanners().getList().then((res) {
      setState(() {
        imgList = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
          success: false,
          title: '데이터 로딩 실패',
          subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      BotToast.closeAllLoading();
    });
  }

  List<ProductsDetailItem> productsListAppliance = [];

  Future<void> _getProductsList(String category) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoProducts().getList(category).then((res) {
      setState(() {
        productsListAppliance = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getBannerList();
    _getProductsList('APPLIANCE');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
         child: Column(
         children: [
            Container(
              child: CarouselSlider(
                  options: CarouselOptions(
                    autoPlay: true,
                 ),
                items: imgList
                    .map((item) => ComponentBannerItem(imgUrl: item))
                    .toList()
              ),
          ),
           Text('IT, 가전'),
           SingleChildScrollView(
             scrollDirection: Axis.horizontal,
             child: Row(
               children: [
                 for(int i = 0; i < productsListAppliance.length; i++)
                   ComponentProduct(imageName: productsListAppliance[i].imageAddress, textName: productsListAppliance[i].productName, productPrice: productsListAppliance[i].disCountPrice)
               ],
             ),
           )
        ],
      ),
    ));
  }
}
