import 'package:flutter/material.dart';
import 'package:shopping_mall_test/function/token_lib.dart';

class PageUserPage extends StatefulWidget {
  const PageUserPage({Key? key}) : super(key: key);

  @override
  State<PageUserPage> createState() => _PageUserPageState();
}

class _PageUserPageState extends State<PageUserPage> {

  String? memberName;

  Future<void> _getMemberName() async {
    String? resultName = await TokenLib.getMemberName();
    setState(() {
      memberName = resultName;
    });
  }

  Future<void> _logout(BuildContext context) async {
    TokenLib.logout(context);
  }

  @override
  void initState() {
    super.initState();
    _getMemberName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 100,
            ),
            Container(
             width: MediaQuery.of(context).size.width,
             height: 100,
             child: Text('$memberName님 즐거운 쇼핑 되세요!', textAlign: TextAlign.center,),
            ),
            OutlinedButton(
                onPressed: () {
                  _asyncConfirmDialog(context);
                },
                child: Text('로그아웃', textAlign: TextAlign.center,)
            ),
          ],
        ),
      )
    );
  }

  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('로그아웃'),
          content: Text('로그아웃 하시겠습니까?'),
          actions: <Widget>[
            TextButton(
              child: Text('취소'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('확인'),
              onPressed: () {
                _logout(context);
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );
  }
}
