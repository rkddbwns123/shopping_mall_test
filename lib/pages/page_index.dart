import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:shopping_mall_test/model/products_response.dart';
import 'package:shopping_mall_test/pages/page_sale_product.dart';
import 'package:shopping_mall_test/pages/page_shopping_basket.dart';
import 'package:shopping_mall_test/pages/page_shopping_product.dart';
import 'package:shopping_mall_test/pages/page_user_page.dart';
import 'package:shopping_mall_test/repository/repo_products.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _currentIndex = 0;
  ProductsResponse? _response;

  @override
  void initState() {
    super.initState();
    _getList('APPLIANCE');
  }

  Future<void> _getList(String category) async{
    await RepoProducts().getList(category).then((res) {
      setState(() {
        _response = res;
      });
    }).catchError((err) {
      print(err);
    }
    );
  }

  final List<BottomNavyBarItem> _navItems = [
    BottomNavyBarItem(
      icon: const Icon(Icons.local_grocery_store),
      title: const Text('상품'),
      activeColor: Colors.red,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.monetization_on),
      title: const Text('할인 상품'),
      activeColor: Colors.purpleAccent,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.favorite),
      title: const Text('장바구니'),
      activeColor: Colors.pink,
      textAlign: TextAlign.center,
    ),
    BottomNavyBarItem(
      icon: const Icon(Icons.person),
      title: const Text('마이 페이지'),
      activeColor: Colors.blue,
      textAlign: TextAlign.center,
    ),
  ];

  final List<Widget> _widgetPages = [
    PageShoppingProduct(),
    PageSaleProduct(),
    PageShoppingBasket(),
    PageUserPage()
  ];

  void _onItemTap(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('ABC 쇼핑몰'),
      ),
      body: SafeArea(
        child: _widgetPages.elementAt(_currentIndex),
      ),
      bottomNavigationBar: BottomNavyBar(
        selectedIndex: _currentIndex,
        showElevation: true,
        itemCornerRadius: 24,
        curve: Curves.easeIn,
        onItemSelected: _onItemTap,
        items: _navItems,
      ),
    );
  }
}