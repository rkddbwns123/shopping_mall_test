import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:shopping_mall_test/components/common/component_custom_loading.dart';
import 'package:shopping_mall_test/components/common/component_notification.dart';
import 'package:shopping_mall_test/components/component_shopping_basket_item.dart';
import 'package:shopping_mall_test/model/shopping_basket_item.dart';
import 'package:shopping_mall_test/repository/repo_shopping_baskets.dart';

class PageShoppingBasket extends StatefulWidget {
  const PageShoppingBasket({Key? key}) : super(key: key);

  @override
  State<PageShoppingBasket> createState() => _PageShoppingBasketState();
}

class _PageShoppingBasketState extends State<PageShoppingBasket> {

  List<ShoppingBasketItem> basketItems = [];

  Future<void> _getShoppingBasketList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoShoppingBasket().getList().then((res) {
      setState(() {
        basketItems = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();

      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getShoppingBasketList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('장바구니', style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
      ),
      body: ListView(
        children: [
          Column(
            children: [
              for(int i = 0; i < basketItems.length; i++)
                ComponentShoppingBasketItem(shoppingBasketItem: basketItems[i]),
              OutlinedButton(
                  onPressed: () {},
                  child: Text('결제하기', style: TextStyle(fontWeight: FontWeight.bold))
              ),
            ],
          ),
        ]
      )
    );
  }
}
