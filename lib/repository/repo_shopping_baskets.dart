import 'package:dio/dio.dart';
import 'package:shopping_mall_test/config/config_api.dart';
import 'package:shopping_mall_test/model/shopping_basket_list_result.dart';

class RepoShoppingBasket {
  Future<ShoppingBasketListResult> getList() async {

    const String baseUrl = '$apiUri/shopping/basket/all/member-id/1';
    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return ShoppingBasketListResult.fromJson(response.data);
  }
}