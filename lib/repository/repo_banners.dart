import 'package:dio/dio.dart';
import 'package:shopping_mall_test/config/config_api.dart';
import 'package:shopping_mall_test/model/banners_response.dart';

class RepoBanners {
  Future<BannersResponse> getList() async {
    const String baseUrl = '$apiUri/banner/info/use';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BannersResponse.fromJson(response.data);
  }
}