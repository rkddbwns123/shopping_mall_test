import 'package:dio/dio.dart';
import 'package:shopping_mall_test/config/config_api.dart';
import 'package:shopping_mall_test/model/products_response.dart';

class RepoProducts {
  Future<ProductsResponse> getList(String category) async {
    const String baseUrl = '$apiUri/product/info/category';

    Map<String, dynamic> params = {};
    params['category'] = category;

    Dio dio = Dio();

    final response = await dio.get(
      baseUrl,
      queryParameters: params,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return ProductsResponse.fromJson(response.data);
  }
}